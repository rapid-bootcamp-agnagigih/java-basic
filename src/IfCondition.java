import java.util.Scanner;

public class IfCondition {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        sampleConvert(scanner);
        System.out.println("Masukkan Angka: ");
        String input = scanner.next();
        int nilai = stringConverter(input);
        /**
         * nilai 0 - 20 -> E
         * nilai 21 - 40 -> D
         * nilai 41 - 60 -> C
         * nilai 61 - 80 -> B
         * nilai 81 - 100 -> A
         */
        char qualitatif = nilaiQualitatif(nilai);
        System.out.println("Nilai Qualitatifnya : " + qualitatif);
    }

    public static char nilaiQualitatif(int input) {
        char qualitatif;
        if (input > 80 && input <= 100) {
            qualitatif = 'A';
        } else if (input > 60) {
            qualitatif = 'B';
        } else if (input > 40) {
            qualitatif = 'C';
        } else if (input > 20) {
            qualitatif = 'D';
        } else if (input >= 0) {
            qualitatif = 'E';
        } else {
            qualitatif = 'K';
        }
        return qualitatif;
    }

    public static Integer stringConverter(String input) {
        int result = 0;
        try{
            result = Integer.parseInt(input);
        }catch (Exception e) {
            System.out.println("Error : " + e.getMessage());
        }
        return result;
    }

    public static void sampleConvert(Scanner scanner){
        // input pertama
        System.out.println("Masukkan angka : ");
        String input1 = scanner.next();
        System.out.println("Hasiil Angka: " + input1);
        // input kedua
        System.out.println("Masukkan kata : ");
        String input2 = scanner.next();
        System.out.println("Hasil kata: " + input2);

        System.out.println("convert ...");
        int angka1 = 0;
        int angka2 = 0;
        try{
            angka1 = Integer.parseInt(input1);
            angka2 = Integer.parseInt(input2);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        System.out.println("hitung ...");
        int hasil = angka1 + angka2;
        System.out.println("Hasil Jumlah " + hasil);
    }
}
