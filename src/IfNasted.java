public class IfNasted {
    public static void main(String[] args) {
        nestedIf(10);
        nestedIf(15);
        nestedIf(10);
        paralelIf(15);
    }

    public static void nestedIf(int angka){
        if(angka > 0) {
            if(angka % 2 == 0){
                int result = angka*4;
                System.out.println("Hasil : " + result);
            } else {
                int result = angka * 3;
                System.out.println("Hasil : " + result);
            }
        } else {
            System.out.println("Angka kurang dari 0");
        }
    }

    public static void paralelIf(int angka) {
        if(angka > 0) {
            System.out.println("Angka lebih dari 0");
        }
        if (angka % 2 == 0) {
            System.out.println("Angka genap");
        } else {
            System.out.println("Angka ganjil");
        }

        if (angka / 2 >= 5) {
            System.out.println("Angka di atas 10");
        } else {
            System.out.println("Angka kurang dari 10");
        }
    }
}
